# Doity Agenda - Cakephp 3

1. Dados de acesso: admin / admin

## Executando

Clone o projeto, o mesmo já possui o diretório /vendor

```bash
git clone https://gitlab.com/Smallkan/doity-agenda.git
```

Importe o backup do banco de dados

```bash
_extras/doity_agenda.sql
```

Configure o arquivo de conexão `'app.php'`

```bash
Linha 266
```

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Demais

Projeto para testes, Doity Agenda.
