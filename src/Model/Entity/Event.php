<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Event Entity
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property int|null $category_id
 * @property int|null $area_id
 * @property int|null $type_id
 * @property string|null $banner
 * @property \Cake\I18n\FrozenTime|null $start_date
 * @property \Cake\I18n\FrozenTime|null $end_date
 * @property \Cake\I18n\FrozenTime|null $start_time
 * @property \Cake\I18n\FrozenTime|null $end_time
 * @property string $local
 * @property string $site
 * @property string $endereco
 * @property int $cep
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property int|null $user_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Area $area
 * @property \App\Model\Entity\Type $type
 * @property \App\Model\Entity\User $user
 */
class Event extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'slug' => true,
        'description' => true,
        'category_id' => true,
        'area_id' => true,
        'type_id' => true,
        'banner' => true,
        'start_date' => true,
        'end_date' => true,
        'start_time' => true,
        'end_time' => true,
        'local' => true,
        'site' => true,
        'endereco' => true,
        'cep' => true,
        'bairro' => true,
        'cidade' => true,
        'estado' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'category' => true,
        'area' => true,
        'type' => true,
        'user' => true
    ];
}
