<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Network\Exception\NotFoundException;
use Cake\Utility\Text;
use Cake\Core\Configure;
use Cake\Network\Session\DatabaseSession;
use Cake\Event\Event;
use Cake\View\Helper\TimeHelper;

/**
 * Events Controller
 *
 * @property \App\Model\Table\EventsTable $Events
 *
 * @method \App\Model\Entity\Event[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EventsController extends AppController
{
    public static $username;

    public function initialize(){
        parent::initialize();
    }


    public function beforeFilter(Event $event){
      $this->Auth->allow();
      $username = (null !== $this->request->session()->read('Auth.User')) ? $this->request->session()->read('Auth.User') : false;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $dados['User'] = $this->Auth->user() ? $this->Auth->user() : false;
        $dados['Areas'] = $this->Areas->find('all')->hydrate(0)->toArray(0);

        $dados['Events'] = $this->Events->find('all');
        $dados['Eventos'] = $this->Events->find('all', ['contain' => ['Categories', 'Areas', 'Types'], 'limit' => 12, 'order' => 'Events.id DESC'])
                                            ->hydrate(0)
                                            ->toArray(0);
        
        $dados['Help'] = [
            'pathUpload' => [
                'banner' => Router::url('/upload/banner/', true)
            ],
            'pathGeral' => Router::url('/images', true)
        ];

        $this->set(compact('dados'));
    }

    /**
     * View method
     *
     * @param string|null $id Event id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $event = $this->Events->get($id, [
            'contain' => ['Categories', 'Areas', 'Types', 'Users']
        ]);

        $this->set('event', $event);
    }

    /**
     * Edit method
     *
     * @param string|null $id Event id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $event = $this->Events->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $event = $this->Events->patchEntity($event, $this->request->getData());
            if ($this->Events->save($event)) {
                $this->Flash->success(__('The event has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The event could not be saved. Please, try again.'));
        }
        $categories = $this->Events->Categories->find('list', ['limit' => 200]);
        $areas = $this->Events->Areas->find('list', ['limit' => 200]);
        $types = $this->Events->Types->find('list', ['limit' => 200]);
        $users = $this->Events->Users->find('list', ['limit' => 200]);
        $this->set(compact('event', 'categories', 'areas', 'types', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Event id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $event = $this->Events->get($id);
        if ($this->Events->delete($event)) {
            $this->Flash->success(__('The event has been deleted.'));
        } else {
            $this->Flash->error(__('The event could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
