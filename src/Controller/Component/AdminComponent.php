<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Utility\Text;

class AdminComponent extends Component {


    //Função para upload de imagem
    public function uploadImage($data, $title, $prefix = ''){
        $extPermited     = ['jpg', 'jpeg', 'gif', 'png'];
        $imageExt        = substr(strtolower(strrchr($data['name'], '.')), 1);
        $imageFileName   = strtolower(Text::slug($title, '_') . '__' . $prefix);

        if (in_array($imageExt, $extPermited)) {
					if ($prefix == 'banner') {
						copy($data['tmp_name'], WWW_ROOT . '/upload/image/banner/' . $imageFileName . '.' . $imageExt);
					} else {
						copy($data['tmp_name'], WWW_ROOT . '/upload/image/' . $imageFileName . '.' . $imageExt);
					}
        }

        return $imageFileName . '.' . $imageExt;
    }

}