<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Event\Event;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Network\Http\Client;

/**
 * Events Controller
 *
 * @property \App\Model\Table\EventsTable $Events
 *
 * @method \App\Model\Entity\Event[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */

class DashboardController extends AppController
{
    public $helpers = [
        'Paginator' => ['templates' => 'paginator-templates']
    ];
    public $paginate = [
        'limit' => 3,
        'order' => [
            'Events.id' => 'DESC'
        ]
    ];

    public function initialize(){
        parent::initialize();

        $this->viewBuilder()->setLayout('dashboard');
    }

    public function index()
    {
        $dados['User'] = $this->Auth->user();
        $dados['Events'] = $this->Events->find('all')->where(['user_id', $dados['User']['id']]);

        $dados['Help'] = [
            'pathUpload' => [
                'banner' => Router::url('/upload/banner/', true)
            ],
            'pathGeral' => Router::url('/images', true)
        ];

        $this->set('title_for_layout', 'Dashboard');
        $this->set(compact('dados'));
    }

    // event_add
    public function eventAdd()
    {
        $dados['User'] = $this->Auth->user();

        $event = $this->Events->newEntity();
        if ($this->request->is('post')) {
            $event = $this->Events->patchEntity($event, $this->request->getData());
            $event->user_id = $dados['User']['id'];
            $event->slug = strtolower(Text::slug($event->title, '-'));
            $event->created = date('Y-m-d H:i:s', time());
            $event->status = 0;

            if (isset($this->request->data['banner']['name']) && !empty($this->request->data['banner']['name'])) {
                $event->banner = $this->Admin->uploadImage($this->request->data['banner'], $event->title, $prefix = 'banner');
            }

            if ($this->Events->save($event)) {
                $this->Flash->success(__('The event has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The event could not be saved. Please, try again.'));
        }
        $categories = $this->Categories->find('all')->hydrate(0)->toArray(0);
        $areas = $this->Areas->find('all')->hydrate(0)->toArray(0);
        $types = $this->Types->find('all')->hydrate(0)->toArray(0);
        $users = $this->Users->find('all')->hydrate(0)->toArray(0);

        $this->set(compact('event', 'categories', 'areas', 'types', 'users','dados'));
    }
}
