<?php
use Cake\Routing\Router;

$pathImage = Router::url('/', true);
?>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <?= $this->Html->charset() ?>
        <title>Doity Agenda <?= isset($title_for_layout) ? ' - ' . $title_for_layout : ''; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <?= $this->fetch('meta') ?>
        <?= $this->Html->css('../libs/bootstrap-select/bootstrap-select.min.css') ?>
        <?= $this->Html->css('libs/c3/c3.min.css') ?>
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('icons.min.css') ?>
        <?= $this->Html->css('app2.min.css') ?>

    </head>

    <body>
        <!-- Navigation Bar-->
        <header id="topnav">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="dropdown notification-list">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>
    
                        <li class="notification-list">
                            <h4><a href="">CONHEÇA A DOITY</a></h4>
                        </li>
                        <li class="notification-list">
                            <h4><a href="">BLOG</a></h4>
                        </li>

                        <li class="notification-list">
                        <button onclick="window.location.href='<?= $pathImage.'dashboard/event/add' ?>'" class="btn btn-light btn-lg rounded-20 mr-3 mt-3">Divulgar Evento</button>
                                
                        </li>

                        <li class="notification-list">
                            <button onclick="window.location.href='<?= $pathImage.'dashboard' ?>'" class="btn btn-danger btn-lg rounded-20 mt-3">Login</button>
                        </li>

                    </ul>

                    <!-- LOGO -->
                    <div class="logo-box">
                        <a href="<?= $pathImage.'dashboard' ?>" class="logo text-center">
                            <span class="logo-lg">
                                <img src="<?= $pathImage.'/images/logo-light.png' ?>" alt="" height="40">
                            </span>
                            <span class="logo-sm">
                                <img src="<?= $pathImage.'/images/logo-light.png' ?>" alt="" height="24">
                            </span>
                        </a>
                    </div>

                </div> <!-- end container-fluid-->

                <div class="explore">
                        <div class="explore-menu">
                            <h1>Explore eventos ao redor do Brasil</h1>
                            <div class="row">
                                <div class="col-8">
                                <form>
                                    <input class="form-control rounded-2" type="search" placeholder="Que tipo de evento você procura?" aria-label="Search">
                                    <button class="invisible" type="submit"></button>
                                </form>
                                </div>
                                <div class="col-4">
                                    <select class="form-control rounded-2" name="area_id" id="area_id">
                                        <?php foreach($dados['Areas'] as $area): ?>
                                            <option value="<?= $area['id'] ?>"><?= $area['name'] ?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>

                            <div class="row negativo">
                                <div class="col-12">
                                    <button class="btn"><img src="<?= $dados['Help']['pathGeral'] ?>/icon-academic.png" alt="" width="45"><br />Acadêmico</button>

                                    <button class="btn"><img src="<?= $dados['Help']['pathGeral'] ?>/icon-academic.png" alt="" width="45"><br />Acadêmico</button>

                                    <button class="btn"><img src="<?= $dados['Help']['pathGeral'] ?>/icon-academic.png" alt="" width="45"><br />Acadêmico</button>

                                    <button class="btn"><img src="<?= $dados['Help']['pathGeral'] ?>/icon-academic.png" alt="" width="45"><br />Acadêmico</button>

                                    <button class="btn"><img src="<?= $dados['Help']['pathGeral'] ?>/icon-academic.png" alt="" width="45"><br />Acadêmico</button>

                                    <button class="btn"><img src="<?= $dados['Help']['pathGeral'] ?>/icon-academic.png" alt="" width="45"><br />Acadêmico</button>

                                </div>

                            </div>
                        </div>
                </div>
            </div>
            <!-- end Topbar -->

        </header>
        <!-- End Navigation Bar-->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="wrapper">
            <div class="container-fluid">

                <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>

            </div> <!-- end container-fluid -->
        </div>
        <!-- end wrapper -->

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

        <!-- Vendor js -->
        <script src="<?= $pathImage.'js/vendor.min.js' ?>"></script>

        <!-- Bootstrap select plugin -->
        <script src="<?= $pathImage.'libs/bootstrap-select/bootstrap-select.min.js' ?>"></script>

        <!-- plugins -->
        <script src="<?= $pathImage.'libs/c3/c3.min.js' ?>"></script>
        <script src="<?= $pathImage.'libs/d3/d3.min.js' ?>"></script>

        <!-- dashboard init -->
        <script src="<?= $pathImage.'js/pages/dashboard.init.js' ?>"></script>

        <!-- App js -->
        <script src="<?= $pathImage.'js/app.min.js' ?>"></script>
        
    </body>
</html>