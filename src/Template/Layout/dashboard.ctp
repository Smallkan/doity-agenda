<?php
use Cake\Routing\Router;

$pathImage = Router::url('/', true);
?>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <?= $this->Html->charset() ?>
        <title>Doity Agenda <?= isset($title_for_layout) ? ' - ' . $title_for_layout : ''; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <?= $this->fetch('meta') ?>
        <?= $this->Html->css('../libs/bootstrap-select/bootstrap-select.min.css') ?>
        <?= $this->Html->css('libs/c3/c3.min.css') ?>
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('icons.min.css') ?>
        <?= $this->Html->css('app.min.css') ?>

    </head>

    <body>
        <!-- Navigation Bar-->
        <header id="topnav">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="dropdown notification-list">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>
    
                        <li class="dropdown notification-list">
                            <a href="javascript:void(0);" class="nav-link right-bar-toggle waves-effect waves-light">
                                <i class="fe-help-circle noti-icon"></i>
                            </a>
                        </li>

                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <i class="fe-bell noti-icon"></i>
                                <span class="badge badge-danger rounded-circle noti-icon-badge">3</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="m-0">
                                        <span class="float-right">
                                            <a href="" class="text-dark">
                                                <small>Limpar todas</small>
                                            </a>
                                        </span>Notificação
                                    </h5>
                                </div>

                                <div class="slimscroll noti-scroll">

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-warning">
                                            <i class="mdi mdi-bell-outline"></i>
                                        </div>
                                        <p class="notify-details">Updates
                                            <small class="text-muted">Nova atualização disponível</small>
                                        </p>
                                    </a>

                                </div>

                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">
                                    Ver todas
                                    <i class="fi-arrow-right"></i>
                                </a>
                            </div>
                        </li>

                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="<?= $pathImage.'images/users/avatar.jpg' ?>" alt="user-image" class="rounded-circle">
                                <?= $dados['User']['name'] ?><br />
                                <small>Administrador</small>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow m-0">Olá <?= $dados['User']['name'] ?>!</h6>
                                </div>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-settings"></i>
                                    <span>Minha conta</span>
                                </a>

                                <div class="dropdown-divider"></div>

                                <!-- item-->
                                <?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'logout', 'class' => 'dropdown-item notify-item']) ?>

                            </div>
                        </li>

                    </ul>

                    <!-- LOGO -->
                    <div class="logo-box">
                        <a href="<?= $pathImage.'dashboard' ?>" class="logo text-center">
                            <span class="logo-lg">
                                <img src="<?= $pathImage.'/images/logo-light.png' ?>" alt="" height="40">
                            </span>
                            <span class="logo-sm">
                                <img src="<?= $pathImage.'/images/logo-light.png' ?>" alt="" height="24">
                            </span>
                        </a>
                    </div>

                </div> <!-- end container-fluid-->
            </div>
            <!-- end Topbar -->

            <div class="topbar-menu">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="has-submenu">
                                <a href="/dashboard">
                                    Início
                                </a>
                            </li>

                        </ul>
                        <!-- End navigation menu -->

                        <div class="clearfix"></div>
                    </div>
                    <!-- end #navigation -->
                </div>
                <!-- end container -->
            </div>
            <!-- end navbar-custom -->

        </header>
        <!-- End Navigation Bar-->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="wrapper">
            <div class="container-fluid">

                <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>

            </div> <!-- end container-fluid -->
        </div>
        <!-- end wrapper -->

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

        <!-- Vendor js -->
        <script src="<?= $pathImage.'js/vendor.min.js' ?>"></script>

        <!-- Bootstrap select plugin -->
        <script src="<?= $pathImage.'libs/bootstrap-select/bootstrap-select.min.js' ?>"></script>

        <!-- plugins -->
        <script src="<?= $pathImage.'libs/c3/c3.min.js' ?>"></script>
        <script src="<?= $pathImage.'libs/d3/d3.min.js' ?>"></script>

        <!-- dashboard init -->
        <script src="<?= $pathImage.'js/pages/dashboard.init.js' ?>"></script>

        <!-- App js -->
        <script src="<?= $pathImage.'js/app.min.js' ?>"></script>
        
    </body>
</html>