<?php
use Cake\Routing\Router;

$pathImage = Router::url('/', true);
?>
<!-- start page title -->
<div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <button class="btn btn-danger btn-lg" onclick="window.location.href='<?= $pathImage.'dashboard/event/add' ?>'">Divulgar evento</button>
                            </div>
                            <h4 class="page-title">Meus Eventos</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title --> 

                <div class="row">
                <?php foreach($dados['Events'] as $evento): ?>
                    <div class="col-xl-6 col-md-12">
                        <div class="card widget-box-two border-light">
                            <div class="card-body">
                                <div class="float-right avatar-lg rounded-circle bg-soft-light mt-2">
                                    <i class="mdi mdi-chart-line font-22 avatar-title text-black"></i>
                                </div>
                                <div class="wigdet-two-content">
                                    <p class="mt-4 text-uppercase text-black" title="Statistics"><?= $evento->start_date ?></p>
                                    <h2 class="text-black mt-2"><span><?= $evento->title ?></span></h2>
                                    <p class="text-black mt-4"><?= $evento->endereco ?><br />
                                                                <?= $evento->cidade ?>, <?= $evento->estado ?></p>
                                </div>
                                <div class="float-right avatar-lg rounded-circle bg-soft-dark">
                                    <i class="fas fa-eye font-8 avatar-title text-black"></i>
                                </div>
                                <div class="float-right avatar-lg rounded-circle bg-soft-dark mr-2">
                                    <i class="fas fa-edit font-8 avatar-title text-black"></i>
                                </div>
                            </div>
                        </div>
                    </div><!-- end col -->
                <?php endforeach; ?>
                </div>
                <!-- end row -->