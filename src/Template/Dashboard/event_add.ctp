<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Event $event
 */

use Cake\Routing\Router;

$pathImage = Router::url('/', true);

?>
<!-- start page title -->
<div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Eventos</a></li>
                                    <li class="breadcrumb-item active">Adicionar Eventos</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Adicionar Eventos</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title --> 

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-6">
                                    <?= $this->Form->create($event);
                                        $this->Form->templates(
                                            ['dateWidget' => '{{day}}{{month}}{{year}}']
                                        );
                                    ?>
                                            <div class="form-group">
                                                <label>Nome do Evento*</label>
                                                <?= $this->Form->input('title', ['type' => 'text', 'label' => false, 'class' => 'form-control', 'placeholder' => 'Nome do Evento']) ?>
                                            </div>
                                            <div class="form-group">
                                                <label>Descrição*</label>
                                                <textarea name="description" id="description" class="form-control" rows="8" placeholder="Descrição"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label>Categorias</label>
                                                        <select class="form-control" name="category_id" id="category_id">
                                                        <?php foreach($categories as $categoria): ?>
                                                            <option value="<?= $categoria['id'] ?>"><?= $categoria['name'] ?></option>
                                                        <?php endforeach;?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label>Área</label>
                                                        <select class="form-control" name="area_id" id="area_id">
                                                        <?php foreach($areas as $area): ?>
                                                            <option value="<?= $area['id'] ?>"><?= $area['name'] ?></option>
                                                        <?php endforeach;?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Banner*</label>
                                                        <div class="containere">
                                                            <img src="<?= $event->banner ? $pathImage.$event->banner : $pathImage.'images/evento.jpg' ?>" alt="evento" style="width:100%">
                                                            <?= $this->Form->input('banner', ['type' => 'file', 'label' => false, 'class' => 'btn']) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div> <!-- end col -->
                                </div>
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        <label>Início*</label>
                                        <input type="datetime-local" name="start_date" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                    <label>Término*</label>
                                        <input type="datetime-local" name="end_date" class="form-control">
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-md-4">
                                            <div class="form-group">
                                            <label>Tipo</label>
                                                <select class="form-control" name="type_id" id="type_id">
                                                <?php foreach($types as $tipo): ?>
                                                    <option value="<?= $tipo['id'] ?>"><?= $tipo['name'] ?></option>
                                                <?php endforeach;?>
                                                </select>
                                            </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Local*</label>
                                                <?= $this->Form->input('local', ['type' => 'text', 'label' => false, 'class' => 'form-control', 'placeholder' => '']) ?>
                                                </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Site*</label>
                                                <?= $this->Form->input('site', ['type' => 'text', 'label' => false, 'class' => 'form-control', 'placeholder' => 'exemplo.com.br']) ?>
                                            </div>
                                    </div> <!-- end col -->
                                </div>
                                <!-- end row -->
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                        <label>Endereço</label>
                                        <?= $this->Form->input('endereco', ['type' => 'text', 'label' => false, 'class' => 'form-control', 'placeholder' => 'Av. Rua, Logradouro, 928']) ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                        <label>CEP</label>
                                        <?= $this->Form->input('cep', ['type' => 'number', 'label' => false, 'class' => 'form-control', 'placeholder' => '57036390']) ?>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                        <label>Bairro</label>
                                        <?= $this->Form->input('bairro', ['type' => 'text', 'label' => false, 'class' => 'form-control', 'placeholder' => 'Jatiúca']) ?>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                        <label>Cidade</label>
                                        <?= $this->Form->input('cidade', ['type' => 'text', 'label' => false, 'class' => 'form-control', 'placeholder' => 'Maceió']) ?>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                        <label>Estado</label>
                                        <?= $this->Form->input('estado', ['type' => 'text', 'label' => false, 'class' => 'form-control', 'placeholder' => 'Alagoas']) ?>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="row text-center">
                                    <div class="col-md-12">
                                        <?= $this->Form->button(__('Adicionar evento'), ['class' => 'btn btn-danger btn-lg']) ?>
                                    </div>
                                </div>
                                <?= $this->Form->end() ?>
                            </div> <!-- end card-body -->
                        </div> <!-- end card -->
                    </div> <!-- end col -->
                </div> <!-- end row --> 
