<?php
use Cake\Routing\Router;

$pathImage = Router::url('/', true);
?>
<!-- start page title -->
<div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Foram encontrados <?= count($dados['Eventos']) ?> resultados para '<span class="text-danger">uns eventos aí</span>:</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title --> 

                <div class="row">
                <?php foreach($dados['Events'] as $evento): ?>
                    <div class="col-xl-4 col-md-8">
                    <div class="card" style="width: 30rem;">
                    <img class="card-img-top" src="<?= $dados['Help']['pathGeral'] ?>/bg_events.jpg" alt="<?= $evento->title ?>">
                    <div class="card-body">
                        <small><?= $evento->created ?></small>
                        <h5 class="card-title"><?= $evento->title ?></h5>
                        <a href="#" class="text-dark">Categoria</a>
                    </div>
                    </div>
                    </div><!-- end col -->
                <?php endforeach; ?>
                </div>
                <!-- end row -->